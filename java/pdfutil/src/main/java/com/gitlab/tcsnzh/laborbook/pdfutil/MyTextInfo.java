package com.gitlab.tcsnzh.laborbook.pdfutil;

public sealed interface MyTextInfo {
    record TextPos(int page, String text, float x, float y, float pageW, float pageH) implements MyTextInfo {
    }

    record Split(int splitCount) implements MyTextInfo {
    }
}
