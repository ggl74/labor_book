declare const APP_GIT_VERSION_COUNT: string;
declare const APP_GIT_NAME_REV: string;
declare const APP_GIT_HEAD: string;
declare const APP_GIT_DIRTY: string[];
declare const APP_BUILD_TIME: string;
