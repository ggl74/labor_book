---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "[Test1]劳动者的魔法书"
  tagline: "[Test1]这是一本劳动者的魔法书。"
  actions:
    - id: health58_test_template1
      theme: brand
      text: "[Test1]健康知识学习实操手册"
      link: /health58_test_template1/01/01_00_00
---
