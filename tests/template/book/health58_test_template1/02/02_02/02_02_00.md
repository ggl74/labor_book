---
internal58:
    authors:
    - 薛伟
    reviewers:
    - 薛伟
---

# 心脏骤停

## 心脏骤停为什么这么可怕？

**心脏骤停是最危重的急症**，如果救治不及时，将发生不可逆转的生物学死亡。

心脏骤停是指心脏停止向脑和其他器官组织泵送血液和氧气。

- **有时，在心脏骤停后立即开始治疗，心肺仍可复苏。但是含氧血液未能泵入大脑的时间越长，患者复苏的可能性就会减小，即使复苏，发生脑损伤的可能性也更大。**

- 如果心脏骤停持续 5 分钟以上而未进行心肺复苏 (CPR) 急救干预（实施见【引用：心肺复苏】），则很可能会造成脑损伤。

- 如果心脏骤停持续 8 分钟以上，则很可能造成死亡。

因此，心脏骤停后必须尽快实施 心肺复苏 (CPR) 急救干预（实施见【引用：心肺复苏】）。

## 为什么会心脏骤停呢

心脏骤停可由任何导致心脏停止跳动的因素引起：

1. 其中一种常见原因是心脏病，以成年人为主要发作人群（冠心病，心律失常，心律异常，器质性心脏病等）。
2. 另一种可能的原因是呼吸停止，例如溺水、出现重度肺部感染或重度哮喘发作。

## 复习

<script setup lang="ts">
import package_json from '../../../../package.json'
</script>

- <a target="_blank" :href="package_json.homepage + '/health58/02/02_01_00.html#'+ encodeURIComponent('三步判断心脏骤停')">如何识别心脏骤停的症状</a>