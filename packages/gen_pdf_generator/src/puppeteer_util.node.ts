import Bottleneck from "bottleneck";
import puppeteer, { PuppeteerLaunchOptions } from "puppeteer";

export const puppeteer_concurrent_limiter = new Bottleneck({
  maxConcurrent: 16,
});

export async function create_browser(
  _obj: { headless: boolean } = { headless: true }
) {
  const puppeteer_option: PuppeteerLaunchOptions = {
    headless: _obj.headless ? "new" : false,
  };
  if (process.env.IS_GITLAB_CI) {
    puppeteer_option.executablePath = "/usr/bin/google-chrome";
    puppeteer_option.args = [
      `--no-sandbox`,
      `--disable-gpu`,
      `--disable-dev-shm-usage`,
      `--disable-web-security`
    ];
  }
  return await puppeteer.launch(puppeteer_option);
}
