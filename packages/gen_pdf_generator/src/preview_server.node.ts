import child_process from "node:child_process";
import { _exist_pth, _kill_child_subprocess } from "./util.node";
import { Writable } from "node:stream";
import { MainContext } from "./main.node";
import { logger } from "@labor-book/utils/src/log";

export type PreviewServer = child_process.ChildProcessByStdio<
  Writable,
  null,
  null
>;

export async function _stop_preview_server(
  nickname: string,
  proc: PreviewServer
) {
  logger.info(
    `Begin stopping the preview server .
    
    It will cause some errors to be displayed in the terminal
    (these errors are output by the child process that will be closed)`
  );
  await _kill_child_subprocess(nickname, proc);
  logger.debug("Success stop preview server");
}

export async function _test_preview_server_running(port: number) {
  try {
    const resp = await fetch(`http://localhost:${port}`);
    return {
      running: true,
      resp,
    };
  } catch (err: any) {
    return {
      running: false,
      err,
    };
  }
}

export async function _start_preview_server(
  ctx_main: MainContext
): Promise<PreviewServer> {
  const { port } = ctx_main._obj.preview_server;
  const { running, err } = await _test_preview_server_running(port);
  if (!running) {
    logger.debug(
      { port, reason: err?.cause?.message },
      `It seems like port not using , It's fine .`
    );
  } else {
    throw new Error(
      "Maybe another preview server running , you can close the `TERMINAL` tab in vscode and it will stop ..."
    );
  }

  logger.debug("Preview server starting ...");

  const preview_server = child_process.spawn(
    /^win32/.test(process.platform) ? "npm.cmd" : "npm",
    [
      "run",
      ctx_main._obj.is_test ? "test:pdf:preview_server" : "pdf:preview_server",
    ],
    {
      stdio: ["pipe", "inherit", "inherit"],
    }
  );

  try {
    logger.debug("Preview server subprocess created ...");

    let retry_count = 0;
    while (true) {
      logger.trace(
        `try fetch preview server , retry count is ${retry_count} .`
      );

      const { running, resp } = await _test_preview_server_running(port);
      if (running) {
        if (resp!!.status != 200)
          throw Error(
            `Maybe launch preview server failed , status not 200 , it's ${
              resp!!.status
            } .`
          );
        else {
          logger.info("Preview server start success ...");
          break;
        }
      } else {
        if (retry_count++ >= 30)
          throw Error("Timeout , maybe launch preview server failed .");
        await new Promise((resolve) => setTimeout(resolve, 1000));
      }
    }
  } catch (err) {
    await _stop_preview_server("preview_server_on_init", preview_server);
    throw err;
  }

  logger.trace("returned preview server child process handle to caller");
  return preview_server;
}
